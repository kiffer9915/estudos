/// <reference types="cypress" />

const entrar = '[data-testid=\'entrar\']'
const alertas = '[role=\'alert\']'
const email = '[data-testid=\'email\']'
const senha = '[data-testid=\'senha\']'

describe('Login', () => {

    beforeEach(() => {
        cy.visit('/login')
        //Verifica se a descrição do login está visível
        cy.contains('h1', 'Login').should('be.visible')
    })

    it.only('Deve clicar em Entrar sem preencher nenhum campo e verificar se os dois alertas são apresentados', () => {

        //Clica em entrar
        cy.get(entrar).click()

        //Verifica se as duas mensagens de alerta são apresentadas
        cy.contains(alertas, 'Email é obrigatório').should('be.visible')
        cy.contains(alertas, 'Password é obrigatório').should('be.visible')

        //Remove os 2 alertas
        cy.get(alertas).find('button').should('have.length', '2').first().click()
        cy.get(alertas).find('button').should('have.length', '1').click()

        //Verifica se não existe mais nenhum alerta
        cy.get(alertas).should('not.exist')

    })

    it.skip('Deve clicar em Entrar preenchendo apenas o e-mail e verificar se o alerta é apresentado', () => {

        //Preenche o email
        cy.get(email).type('kiffer9915@gmail.com').should('have.value', 'kiffer9915@gmail.com')

        //Clica em Entrar
        cy.get(entrar).click()

        //Verifica se a mensagem de alerta é apresentada
        cy.contains(alertas, 'Password é obrigatório').should('be.visible')

        //Remove o alerta
        cy.get(alertas).find('button').should('have.length', '1').click()

        //Verifica se não existe mais nenhum alerta
        cy.get(alertas).should('not.exist')

    });

    it.skip('Deve clicar em Entrar preenchendo apenas a senha e verificar se o alerta é apresentado', () => {

        //Preenche a senha
        cy.get(senha).type('123').should('have.value', '123')

        //Clica em Entrar
        cy.get(entrar).click()

        //Verifica se a mensagem de alerta é apresentada
        cy.contains(alertas, 'Email é obrigatório').should('be.visible')

        //Remove o alerta
        cy.get(alertas).find('button').should('have.length', '1').click()

        //Verifica se não existe mais nenhum alerta
        cy.get(alertas).should('not.exist')

    });

    it.skip('Deve digitar e-mail e senha incorretos e verificar se o alerta é apresentado', () => {

        //Preenche o email
        cy.get(email).type('errado@errado.com').should('have.value', 'errado@errado.com')
        //Preenche a senha
        cy.get(senha).type('123').should('have.value', '123')

        //Clica em Entrar
        cy.get(entrar).click()

        //cy.screenshot({ capture: 'runner'})

        //Verifica se a mensagem de alerta é apresentada
        cy.contains(alertas, 'Email e/ou senha inválidos').should('be.visible')

        //Remove o alerta
        cy.get(alertas).find('button').should('have.length', '1').click()

        //Verifica se não existe mais nenhum alerta
        cy.get(alertas).should('not.exist')

    });

})