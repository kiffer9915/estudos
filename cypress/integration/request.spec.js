/// <reference types= "cypress" />

describe('Request', () => {

    it.only('intercept', () => {

        //cy.intercept('POST', '')

        cy.visit('/login')

        cy.postgreSQL(`select * from public.teste`)

    });

    //Cypress._.times(1, () => {
    it.skip('Teste', () => {

        cy.intercept('GET', 'https://serverest.dev/produtos', {
            body: {
                "quantidade": 2,
                "produtos": [
                    {
                        "nome": "Logitech MX Vertical",
                        "preco": 470,
                        "descricao": "Mouse",
                        "quantidade": 382,
                        "_id": "BeeJh5lz3k6kSIzA"
                    },
                    {
                        "nome": "Samsung 60 polegadas",
                        "preco": 5240,
                        "descricao": "TV",
                        "quantidade": 49977,
                        "_id": "K6leHdftCeOJj8BJ"
                    },
                    {
                        "nome": "teste1",
                        "preco": 1,
                        "descricao": "1",
                        "quantidade": 1,
                        "_id": "1"
                    },
                    {
                        "nome": "teste2",
                        "preco": 2,
                        "descricao": "2",
                        "quantidade": 2,
                        "_id": "2"
                    },
                    {
                        "nome": "teste3",
                        "preco": 3,
                        "descricao": "3",
                        "quantidade": 3,
                        "_id": "3"
                    }
                ]
            }
        }).as('produtos')

        // cy.intercept('GET', 'https://front.serverest.dev/admin/home', (req) => {
        //     req.destroy()
        // })

        cy.intercept('GET', 'https://front.serverest.dev/static/media/serverestlogo1branco.7e020988.png', { fixture: 'teste.png' })

        // cy.intercept('GET', 'https://front.serverest.dev/serveresticon.ico', (req) => {
        //     req.destroy()
        // })

        cy.request({
            method: 'POST',
            url: 'https://serverest.dev/login', // baseUrl é prefixado para URL
            form: true, // indica que o corpo deve ter o formato urlencoded e define Content-Type: application / x-www-form-urlencoded cabeçalhos
            body: {
                email: 'kiffer9915@gmail.com',
                password: '123',
            },
        }).then((resp) => {
            expect(resp.status).to.eq(200)
            console.log(resp)

            const token = resp.body.authorization

            window.localStorage.setItem('serverest/userNome', 'sander')
            window.localStorage.setItem('serverest/userEmail', 'kiffer9915@gmail.com')
            window.localStorage.setItem('serverest/userToken', `${token}`)
        })

        cy.request({
            method: 'POST',
            url: "https://serverest.dev/usuarios",
            //form: true,
            body: {
                "nome":"teste1",
                "email":"aaaaa@sdgsd.com",
                "password":"123",
                "administrador":"false"
             }
             
        }).then((resp) => {
            expect(resp.status).to.eq(201)
        });

        //cy.visit('/admin/listarprodutos')

    });

});